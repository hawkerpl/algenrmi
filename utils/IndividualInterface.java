/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.util.Vector;
        

/**
 *
 * @author hawker
 */
@SuppressWarnings("unused")
public interface IndividualInterface extends java.io.Serializable {
    public boolean mutate(int p);
    public String getGenome();
    public void setGenome(String v);
    public double fitness(String target);
  
}

