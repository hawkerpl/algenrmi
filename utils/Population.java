/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.io.Serializable;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author hawker
 */
@SuppressWarnings("serial")
public class Population implements PopulationInterface, Serializable {

    Vector<Individual> v;
    int mutate=20;
    public Population(int popsize,int genlen,int seed){
        v=new Vector<Individual>();
        for(int i = 0;i<popsize;++i){
            v.add(new Individual(genlen));
            v.lastElement().seed(i+1*24);
        }
    }
    @Override
    public IndividualInterface getBestInd() {
        Collections.sort(this.v);
        return this.v.elementAt(0);
    }
    public void addInd(Individual i){
        v.set(v.size()-1, (Individual)i);
    }
    @Override
    public void countFitness(String target) {
        for(Individual i:this.v){
            i.fitness(target);
        }
    }

    @Override
    public void makeSelection() {
        this.v = Individual.select(this.v);
    }

    @Override
    public void breed() {
        
         Vector<Individual> newpop = new Vector<Individual>() ;
         Vector<Individual> tmp;
        for(int i=0;i<this.v.size()-1;i+=2){
            tmp = Individual.breedTwo(v.elementAt(i),v.elementAt(i+1));
            newpop.add(tmp.elementAt(0));
            newpop.add(tmp.elementAt(1));
    }
        for(Individual i : newpop){
            i.mutate(20);
        }
        this.v=newpop;
    }
    public String toString(){
        return this.v.toString();
    }
}
