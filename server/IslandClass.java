/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import utils.Individual;
import utils.IndividualInterface;
import utils.IslandInterface;
import utils.Population;

/**
 *
 * @author hawker
 */
/***
 * Klasa odpowiadajaca za calosc ewolucji na "wyspie"
 * 
 * @author hawker
 */
@SuppressWarnings("serial")
public class IslandClass extends UnicastRemoteObject implements IslandInterface, Serializable {
	/**
	 * Cel ewolucji
	 */
	String target;
	/**
	 * liczba generacji
	 */
	int gen;
	/**
	 * nasza populacja
	 */
	Population pop;
	/**
	 * czy elitism ?
	 */
	boolean el = false;

	/**
	 * Konstruktor klasy
	 * 
	 * @param rndseed
	 *            seed do Pseudolosowej(ma byc inny dla kazdej klasy)
	 * @param popsize
	 *            licznosc populacji
	 * @param t
	 *            cel ewolucji (string ktory chcemy znalezc)
	 * @param generations
	 *            liczba generacji
	 * @param elitism
	 *            odpowiada za wlaczenie elityzmu
	 * @throws RemoteException 
	 */
	public IslandClass() throws RemoteException {
		super();
	}
        public void setAll(int rndseed, int popsize, String t, int generations, boolean elitism){
		el = elitism;
		target = t;
		pop = new Population(popsize, t.length(), rndseed);
		gen = generations;
	}

	/**
	 * Funkcja w ktorej nastepuje ewolucja, po liczbie generacji "generations"
	 * zwraca najlepszego osobnika jakiego udalo sie znalezc
	 * 
	 * @return IndividualInterface z najlepszym osobnikiem (trzeba rzutowac do
	 *         Individual)
	 * @throws RemoteException
	 */
	@Override
	public IndividualInterface evolve() throws RemoteException {
		Individual best, tmp;
		pop.countFitness(target);
		best = (Individual) pop.getBestInd();
		for (int i = 0; i < this.gen; ++i) {

			pop.countFitness(target);

			tmp = (Individual) pop.getBestInd();
			if (tmp.fitness < best.fitness)
				best = tmp;

			pop.makeSelection();
			pop.breed();
			if (el)
				pop.addInd(tmp);
		}
		tmp = (Individual) pop.getBestInd();
		if (tmp.fitness < best.fitness)
			best = tmp;
		pop.countFitness(target);
		System.out.println(((Individual)pop.getBestInd()).genome);
		return pop.getBestInd();
	}

	/**
	 * Dodaje podanego osobnika do populacji
	 * 
	 * @param i
	 *            osobnik ktory ma byc dodany do populacji
	 * @throws RemoteException
	 */
	@Override
	public void addIndividualToPopulation(IndividualInterface i) throws RemoteException {
		pop.addInd((Individual) i);
	}

}
