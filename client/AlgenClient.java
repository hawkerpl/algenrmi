/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package client;

import utils.Individual;
import utils.IslandInterface;

/**
 *
 * @author hawker
 */
public class AlgenClient implements Runnable {
	public volatile boolean flag;
	public volatile Individual best;
	public volatile IslandInterface islandObject;
	
	public AlgenClient(boolean f, IslandInterface ii) {
		flag = f;
		islandObject = ii;
	}
	
    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {  
    		best = (Individual) islandObject.evolve(); //metoda przed zwroceniem best'a wypisuje jego genome na serwerze
    		System.out.println(best.genome); //wypisanie genome na kliencie
    		flag = true;

    	} catch(Exception e) {
    		System.err.println(e);
    	}
    }
}





