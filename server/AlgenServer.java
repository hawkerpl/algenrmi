/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package server;

import java.rmi.*;  
import java.rmi.registry.*;
import java.rmi.Naming;
import java.util.Random;

/**
 * 
 * @author hawker
 */
@SuppressWarnings("unused")
public class AlgenServer {
	public static void main(String[] args) {
		Random rndseed = new Random();
		int n = Integer.parseInt(args[0]);
		int popsize = Integer.parseInt(args[1]);
		int generations = Integer.parseInt(args[2]);
		String t = args[3];
		boolean elitism = Boolean.valueOf(args[4]);
		//int n = 4;
		//int popsize = 10;
		//int generations = 20;
		//String t = "aaa";
		//boolean elitism = true;

		try {
			IslandClass ic = null;
			for(int i = 0; i < n; ++i) {
				ic = new IslandClass(rndseed.nextInt(), popsize, t, generations, elitism);
				Naming.bind("islandObject" + i, ic);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
