/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import static java.lang.Math.abs;

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 * 
 * @author hawker
 */
@SuppressWarnings("serial")
public class Individual implements IndividualInterface, Comparable<Individual> {

	public final static String genePool = "0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public String genome;
	public double fitness;
	public static Random generator = new Random();

	public Individual(String in) {
		this.genome = in;
	}

	public void seed(int i) {
		generator = new Random(i);
	}

	public Individual(int len) {

		char[] text = new char[len];
		for (int i = 0; i < len; i++) {
			text[i] = genePool.charAt(generator.nextInt(genePool.length()));
		}
		genome = new String(text);
	}

	@Override
	public boolean mutate(int p) {
		if (generator.nextInt(p) == 0) {
			StringBuilder tmp;
			tmp = new StringBuilder(this.genome);
			tmp.setCharAt(generator.nextInt(this.genome.length()),
					genePool.charAt(generator.nextInt(genePool.length())));
			this.genome = tmp.toString();
			return true;
		} else
			return false;
	}

	@Override
	public String getGenome() {
		return this.genome;
	}

	@Override
	public void setGenome(String v) {
		this.genome = v;
	}

	@Override
	public double fitness(String target) {
		double sum = 0;
		for (int i = 0; i < this.genome.length(); i++) {
			int len = abs(genePool.indexOf(this.genome.charAt(i))
					- genePool.indexOf(target.charAt(i)));
			sum += Math.pow(len, 2);
		}
		this.fitness = sum;
		return sum;
	}

	public static char gaussianChar(char a, char b) {
		@SuppressWarnings("unused")
		int odl = abs(genePool.indexOf(a) - genePool.indexOf(b));
		int gval;
		int mean = genePool.indexOf(a) + genePool.indexOf(b) / 2;

		int stdev = abs(mean - genePool.indexOf(b));
		/*
		 * if (odl<=1) gval = mean+generator.nextInt(3)-1; else
		 */gval = (int) (generator.nextGaussian() * stdev + mean);
		if (gval < 0)
			gval = 0;
		if (gval >= genePool.length())
			gval = genePool.length() - 1;
		return genePool.charAt(gval);
	}

	public static Vector<Individual> breedTwo(Individual a, Individual b) {
		Vector<Individual> newPair = new Vector<Individual>();
		int crsspoint = generator.nextInt(a.genome.length());
		StringBuilder strA = new StringBuilder(a.genome);
		StringBuilder strB = new StringBuilder(b.genome);
		for (int i = 0; i < a.genome.length(); ++i) {
			if (i <= crsspoint) {
				strA.setCharAt(i,
						gaussianChar(a.genome.charAt(i), b.genome.charAt(i)));
				strB.setCharAt(i,
						gaussianChar(a.genome.charAt(i), b.genome.charAt(i)));
			}

		}
		newPair.add(new Individual(strA.toString()));
		newPair.add(new Individual(strB.toString()));

		return newPair;
	}

	public static Vector<Individual> select(Vector<Individual> v) {

		Vector<Double> d = new Vector<Double>();
		Collections.sort(v);
		Vector<Individual> newpop = new Vector<Individual>();
		double sum = 0;
		for (Individual i : v) {
			sum += v.lastElement().fitness - i.fitness;
			d.add(sum);
		}
		for (int i = 0; i < d.size(); ++i) {
			double r = generator.nextFloat() * sum;
			for (int j = 0; j < d.size(); ++j) {
				if (j == 0 && r >= 0 && r <= d.elementAt(j)) {
					newpop.add(v.elementAt(j));
					break;
				}
				if (j != 0 && r > d.elementAt(j - 1) && r < d.elementAt(j)) {
					newpop.add(v.elementAt(j));
					break;
				}

			}
		}
		return newpop;
	}

	public String toString() {
		return "G:" + this.genome + " F:" + this.fitness;
	}

	@Override
	public int compareTo(Individual t) {
		return (int) (this.fitness - t.fitness);
	}

}
