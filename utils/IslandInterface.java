/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.rmi.*;

import utils.IndividualInterface;

/**
 * 
 * @author hawker
 */

public interface IslandInterface extends Remote {

	public void addIndividualToPopulation(IndividualInterface i) throws RemoteException;

	public IndividualInterface evolve() throws RemoteException;

}
