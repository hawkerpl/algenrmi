/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

/**
 *
 * @author hawker
 */
public interface PopulationInterface {
    public IndividualInterface getBestInd();
    public void countFitness(String target);
    public void makeSelection();
    public void breed();
}
